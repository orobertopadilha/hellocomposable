package br.edu.unisep.hellocompose

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.ui.platform.setContent
import br.edu.unisep.hellocompose.theme.HelloComposeTheme
import br.edu.unisep.hellocompose.ui.MainView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            HelloComposeTheme {
                MainView(::sayHello)
            }
        }
    }

    private fun sayHello() =
        Toast.makeText(this, "Hello, Compose!", Toast.LENGTH_LONG).show()

}