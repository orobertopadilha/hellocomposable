package br.edu.unisep.hellocompose.ui

import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.ui.tooling.preview.Preview

@Composable
fun MainView(onClickMe: () -> Unit = {}) {
    Column(
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {

        Row {
            Text(text = "Hello, Composable!")
            Spacer(modifier = Modifier.width(16.dp))
            Text(text = "This is an UI example!")
        }

        Spacer(modifier = Modifier.height(32.dp))

        Button(
            onClick = {
                onClickMe()
            },
            modifier = Modifier
                .fillMaxWidth()
        ) {
            Text(text = "Click me!")
        }
    }
}

@Preview(showBackground = true)
@Composable
fun MainViewPreview() {
    MainView()
}